# Quattro default application

## Nginx
A simple static-content application.

## Build
docker build -t quattro-default-app .

## Usage
docker run --name quattro-default-app -p 8080:80 -d quattro-default-app
